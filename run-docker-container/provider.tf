terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.15.0"
    }
  }
}

provider "docker" {}

resource "docker_container" "nginx" {
  image = "nginx:1.23.3"
  name  = "tutorial"
  ports {
    internal = 80
    external = 7878
  }
}