terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
  
}

# Create new subdomain named test-binar.taiga.run
resource "cloudflare_record" "test-binar" {
    zone_id = var.cloudflare_zone_id
    name = "test-binar"
    value = "127.0.0.1"
    type = "A"
}